<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%post_cat}}`.
 */
class m160516_034114_create_post_cat extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%post_cat}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
            'id_lang' => $this->integer(11),
            'id_user' => $this->integer(11),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
        $this->insert('{{%post_cat}}',array('title' => '1st English category', 'id_lang' => '1', 'id_user' => '1','created_at' => time() ));
        $this->insert('{{%post_cat}}',array('title' => '1-я Русскоязычная категория', 'id_lang' => '2', 'id_user' => '1','created_at' => time()));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%post_cat}}');
    }
}
