<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%lang}}`.
 */
class m160516_034051_create_lang extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%lang}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
            'code' => $this->string(10),
        ]);
        $this->insert('{{%lang}}',array('title' => 'English', 'code' => 'en-US'));
        $this->insert('{{%lang}}',array('title' => 'Русский', 'code' => 'ru-RU'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%lang}}');
    }
}
