<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace common\widgets;

use Yii;
use yii\helpers\Html;

/**
 * LanguageSelector widget renders a dropdown list for language select
 * in the sequence they were assigned using setFlash. You can set message as following:
 *
 * @author Helen Lazar <helen.lazar.77@gmail.com>
 */
class LanguageSelector extends \yii\bootstrap\Widget
{

    public function init()
    {
        parent::init();

        echo Html::beginForm(['/site/language'], 'post', ['id'=>'lang_sel_form'])
        . Html::DropdownList('language', [Yii::$app->language],
        array_reduce(
          Yii::$app->params['supportedLanguages'],
          function($carry, $item){
            $carry[$item] = $item;
            return $carry;
          },
          []
        )
        , ['onchange' => "this.form.submit()"])
        . Html::endForm();
    }
}
