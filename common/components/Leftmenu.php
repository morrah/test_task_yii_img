<?php
namespace common\components;

use Yii;
use common\models\Page;
use yii\helpers\Html;
use yii\helpers\Url;

class Leftmenu {

  public static function makeMenuItems($entities_names=[]){
    /*if(! is_array($entities_names)){
      if(strlen(trim(stval($entities_names)))){
        $entities_names = [trim(stval($entities_names))]
      }
    }
    if(!count($entities_names)){
      entities_names=['page'];
    }
    foreach($entities_names as $ename){

    }*/
    $items = [];
    $items[] = ['label' => Yii::t('page', 'Static pages'), 'url' => ['/site/pages'],
      'items' => [
        ['label' => Yii::t('page', 'Useful'), 'url' => ['/site/pages/useful']],
        ['label' => Yii::t('page', 'Unuseful'), 'url' => ['/site/pages/unuseful']],
      ]
    ];
    $item = ['label' => Yii::t('page', 'Pages'), 'url' => Url::toRoute('/page')];
    //$id_lang = Lang::findOne(['code'],Yii::$app->language)
    $pages = Page::find()
    ->select(['id as id', 'title as label'])
    //->andFilterWhere(['id_lang', $id_lang])
    //->andFilterWhere(['active', 1])
    ->orderBy('created_at DESC')
    ->limit(10)
    ->asArray()
    ->all();
    $item['items'] = array_map(
      function($arg){
        return ['label' => $arg['label'], 'url' => Url::toRoute('/page/'.$arg['id'])];
      },
      $pages
    );
    array_unshift($item['items'],
      ['label' => Yii::t('page', 'Search Pages'), 'url' => ['/page/search']]
    );
    $items[] = $item;
    $items[] =   ['label' => Yii::t('post-cat', 'All post types'), 'url' => ['/post-cat/index'],
        'items' => [
          ['label' => Yii::t('post-cat', 'Post type 1'), 'url' => ['/post-cat/view/1']],
          ['label' => Yii::t('post-cat', 'Post type 2'), 'url' => ['/post-cat/view/2']],
        ]
      ];
    $items[] =   ['label' => Yii::t('post', 'News'), 'url' => ['/post/news']];
    return $items;
  }

}
