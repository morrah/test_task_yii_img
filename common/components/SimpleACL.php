<?php

namespace common\components;

use Yii;
use yii\base\BootstrapInterface;
use yii\web\ForbiddenHttpException;
use yii\helpers\Url;

class SimpleACL implements BootstrapInterface
{
  /**
   * The simplest way to implement ACL avoiding Yii native RBAC is
   * to add access control check into bootstrap.
   * May be useful when your app has just several permanent roles
   * Check access for specified app IDs, specified users by specified parameters
   * in your common/config/main.php add code like below:
   * 'bootstrap' => [
   * [
   * ...
   * [
   * 'class' => 'common\components\SimpleACL',
   *  'acl' => [
   *    'app-backend' => [ // application ID
   *      'checked_by' => 'id', //application ID, for ex.'app-backend'
   *      'id' => [
   *        1,2,3 // allowed users IDs
   *      ],
   * // or
   *      'group_id' => [
   *       1,2,3 //allowed users groups (User)
   *     ],
   * // or
   *      'is_admin' =>[1],
   *    ],
   *  ]
   * ]
   * ...
   * ],
   * Remember that your User model must have fields or relations using
   * for access control, for ex. 'group_id' or 'is_admin' field
   * @param array $acl
   *
   */

    public $acl = [];

    public $loginUrl;

    public $logoutUrl;

  /**
   * Bootstrap method to be called during application bootstrap stage.
   * @param Application $app the application currently running
   */
  public function bootstrap($app){
    try{
      // Not for console
      if(! $app instanceof yii\web\Application)
        return true;
      // Current application has specified id
      if(!in_array($app->id, array_keys($this->acl)))
        return true;

      $loginUrl = (array) $app->user->loginUrl;
      $this->loginUrl = !empty($this->loginUrl)?$this->loginUrl:($loginUrl[0]);
      $this->logoutUrl = !empty($this->logoutUrl)?$this->logoutUrl:'site/logout';

      // url is logout page url
      if(static::getRoute()==$this->logoutUrl)
        return true;

      // Unauthorized user
      if(!$app->user->id){
        //Redirect to login page
        if(static::getRoute()!=$this->loginUrl && !$app->getResponse()->isRedirection){
          return $app->getResponse()->redirect($this->loginUrl);
        }
      }
      // Authorized user
      else{
        // User doesn't belong to access permitted users
        if(!in_array($app->user->getIdentity()->{$this->acl[$app->id]['checked_by']}, $this->acl[$app->id][$this->acl[$app->id]['checked_by']])){
          if(static::getRoute()!=$this->loginUrl){
            $app->user->logout();
            //throw new ForbiddenHttpException(Yii::t('yii', 'Forbidden!'));
            return;
          }
        }
      }
    }
    catch(Exception $e){
      return;
    }
    return true;
  }

  public static function getRoute()
  {
    if(!Yii::$app->getUrlManager()->enablePrettyUrl)
      return Yii::$app->request->getQueryParam(Yii::$app->getUrlManager()->routeParam);
    return strpos(Yii::$app->request->url,'?') ? substr(Yii::$app->request->url,0,strpos(Yii::$app->request->url,'?')) : Yii::$app->request->url;
  }

}
