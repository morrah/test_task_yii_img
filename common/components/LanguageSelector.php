<?php

namespace common\components;

use Yii;
use yii\base\BootstrapInterface;
use yii\web\Cookie;

class LanguageSelector implements BootstrapInterface
{

    public $supportedLanguages = [];

  /**
   * Bootstrap method to be called during application bootstrap stage.
   * @param Application $app the application currently running
   */
  public function bootstrap($app){
    try{
      if($app instanceof yii\web\Application){
        $preferredLanguage = isset($app->request->cookies['language']) ? (string)$app->request->cookies['language'] : null;
        if (empty($preferredLanguage)) {
          $preferredLanguage = $app->request->getPreferredLanguage($this->supportedLanguages);
        }
        $app->language = $preferredLanguage;
      }
    }
    catch(Exception $e){
      return;
    }
    return true;
  }

  public static function setCookieLanguage($language = null)
  {
    $language = !empty($language)?$language:(Yii::$app->request->post('language'));
    Yii::$app->language = $language;
    $languageCookie = new Cookie([
      'name' => 'language',
      'value' => $language,
      'expire' => time() + 60 * 60 * 24 * 30,
    ]);
    Yii::$app->response->cookies->add($languageCookie);
  }

}
