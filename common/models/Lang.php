<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%lang}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $code
 */
class Lang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%lang}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 255],
            [['code'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('lang', 'ID'),
            'title' => Yii::t('lang', 'Title'),
            'code' => Yii::t('lang', 'Code'),
        ];
    }

    public static function getLangsDropDown(){
      return ArrayHelper::map(static::find()->select('id,code')->asArray()->all(),'id','code');
    }
}
