<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%post}}".
 *
 * @property integer $id
 * @property string $title
 * @property integer $id_lang
 * @property integer $id_post_cat
 * @property string $content
 * @property string $img
 * @property integer $id_user
 * @property integer $created_at
 * @property integer $updated_at
 */
class Post extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%post}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_lang', 'id_post_cat', 'id_user', 'created_at', 'updated_at'], 'integer'],
            [['content', 'img'], 'string'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('post', 'ID'),
            'title' => Yii::t('post', 'Title'),
            'id_lang' => Yii::t('post', 'Id Lang'),
            'id_post_cat' => Yii::t('post', 'Id Post Cat'),
            'content' => Yii::t('post', 'Content'),
            'img' => Yii::t('post', 'Img'),
            'id_user' => Yii::t('post', 'Id User'),
            'created_at' => Yii::t('post', 'Created At'),
            'updated_at' => Yii::t('post', 'Updated At'),
        ];
    }
}
