<?php

namespace common\controllers;

class CommonController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLanguage()
    {
      $language = Yii::$app->request->post('language');
      Yii::$app->language = $language;
      $languageCookie = new Cookie([
        'name' => 'language',
        'value' => $language,
        'expire' => time() + 60 * 60 * 24 * 30,
      ]);
      Yii::$app->response->cookies->add($languageCookie);
      return $this->redirect(Yii::$app->getRequest()->referrer);
    }

}
