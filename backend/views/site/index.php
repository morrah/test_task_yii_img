<?php

/* @var $this yii\web\View */
use yii\widgets\Menu;

$this->title = Yii::t('common', 'Admin interface') . ' | ' . Yii::t('common', 'My Company');
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2><?= Yii::t('common','Menu') ?></h2>

                <?php
                $menuItems = [
                    ['label' => Yii::t('user', 'Users'), 'url' => ['/user/index']],
                    ['label' => Yii::t('lang', 'Languages'), 'url' => ['/lang/index']],
                    ['label' => Yii::t('post-cat', 'Post types'), 'url' => ['/post-cat/index']],
                    ['label' => Yii::t('post', 'Posts'), 'url' => ['/post/index']],
                    ['label' => Yii::t('page', 'Pages'), 'url' => ['/page/index']],
                ];
                echo Menu::widget([
                    'options' => ['class' => 'unstyled'],
                    'items' => $menuItems,
                ]);
                ?>
            </div>
            <div class="col-lg-8">
                <h2><?= Yii::t('common','Main info') ?></h2>

            </div>
        </div>

    </div>
</div>
