<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('user', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('user', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('user', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            ['label' => Yii::t('common', 'Thumbnail image'), 'value' => (!empty($model->thumbnail)?(Html::img($model->thumbnail->getThumbUrl('medium'))):''), 'format' => 'html'],
            'auth_key',
            'password_hash',
            'password_reset_token',
            'email:email',
            'status',
            ['label' => Yii::t('user', 'Group'), 'value' => isset(Yii::$app->params['user_groups'][$model->group_id])?Yii::$app->params['user_groups'][$model->group_id]:null],
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
