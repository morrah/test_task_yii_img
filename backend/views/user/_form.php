<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use pendalf89\filemanager\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['disabled' => 'disabled']) ?>

    <?= $form->field($model, 'email')->textInput(['disabled' => 'disabled']) ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'group_id')->dropDownList(
      Yii::$app->params['user_groups'],
      [
        'prompt'=> Yii::t('user', 'Group'),
        'options' => [
          Yii::$app->params['default_user_group'] => ['selected'=>true],
        ]
      ]
    ) ?>

    <?= $form->field($model, 'thumbnail')->widget(FileInput::className(), [
        'buttonTag' => 'button',
        'buttonName' => 'Browse',
        'buttonOptions' => ['class' => 'btn btn-default'],
        'options' => ['class' => 'form-control'],
        'template' => '<div class="img">'.(!empty($model->thumbnail)?(Html::img($model->thumbnail->getThumbUrl('medium'))):'').'</div><div class="input-group"><span class="invisible">{input}</span><span class="input-group-btn">{button}</span></div>',
        'thumb' => 'original',
        'imageContainer' => '.img',
        'pasteData' => FileInput::DATA_ID,
        'callbackBeforeInsert' => 'function(e, data) {
            console.dir( data );
        }',
    ]);
    ?>

    <?= $form->field($model, 'created_at')->textInput(['disabled' => 'disabled']) ?>

    <?= $form->field($model, 'updated_at')->textInput(['disabled' => 'disabled']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('user', 'Create') : Yii::t('user', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
