<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PostCat */

$this->title = Yii::t('post-cat', 'Update {modelClass}: ', [
    'modelClass' => 'Post Cat',
]) . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('post-cat', 'Post Cats'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('post-cat', 'Update');
?>
<div class="post-cat-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
