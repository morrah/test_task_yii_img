<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Page */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('page', 'Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('page', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('page', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('page', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'id_lang',
            'keywords',
            ['label' => Yii::t('common', 'Thumbnail image'), 'value' => (!empty($model->thumbnail)?(Html::img($model->thumbnail->getThumbUrl('medium'))):''), 'format' => 'html'],
            'content:html',
            ['label' => Yii::t('common', 'Created by'), 'value' => (!empty($model->user)?(Html::a($model->user->username, Url::toRoute(['/user/view', 'id' => $model->id_user]))):''), 'format' => 'html'],
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>

</div>
