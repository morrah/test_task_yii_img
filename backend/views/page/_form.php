<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use pendalf89\filemanager\widgets\TinyMCE;
use pendalf89\filemanager\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\Page */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="page-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_lang')->dropDownList($langs, ['options' => $selected_lang])//textInput() ?>

    <?= $form->field($model, 'keywords')->textInput(['maxlength' => true]) ?>

    <?php /*$form->field($model, 'content')->textarea(['rows' => 6])*/ ?>

    <?= $form->field($model, 'content')->widget(TinyMCE::className(), [
        'clientOptions' => [
          'language' => 'ru',
          'menubar' => false,
          'height' => 500,
          'image_dimensions' => false,
          'plugins' => [
              'advlist autolink lists link image charmap print preview anchor searchreplace visualblocks code contextmenu table',
          ],
          'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | code',
        ],
    ]); ?>

    <?= $form->field($model, 'thumbnail')->widget(FileInput::className(), [
        'buttonTag' => 'button',
        'buttonName' => 'Browse',
        'buttonOptions' => ['class' => 'btn btn-default'],
        'options' => ['class' => 'form-control'],
        // Widget template
        'template' => '<div class="img">'.(!empty($model->thumbnail)?(Html::img($model->thumbnail->getThumbUrl('medium'))):'').'</div><div class="input-group"><span class="invisible">{input}</span><span class="input-group-btn">{button}</span></div>',
        // Optional, if set, only this image can be selected by user
        'thumb' => 'original',
        // Optional, if set, in container will be inserted selected image
        'imageContainer' => '.img',
        // Default to FileInput::DATA_URL. This data will be inserted in input field
        'pasteData' => FileInput::DATA_ID,//DATA_URL,
        // JavaScript function, which will be called before insert file data to input.
        // Argument data contains file data.
        // data example: [alt: "Ведьма с кошкой", description: "123", url: "/uploads/2014/12/vedma-100x100.jpeg", id: "45"]
        'callbackBeforeInsert' => 'function(e, data) {
            console.dir( data );
        }',
    ]);
    ?>

    <?= $form->field($model, 'id_user')->textInput(['disabled' => 'disabled']) ?>

    <?= $form->field($model, 'created_at')->textInput(['disabled' => 'disabled']) ?>

    <?= $form->field($model, 'updated_at')->textInput(['disabled' => 'disabled']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('page', 'Create') : Yii::t('page', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
