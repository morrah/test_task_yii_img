<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = $title;
$this->params['breadcrumbs'][] = $this->title;
$this->params['leftMenu'] = isset($leftMenu)?$leftMenu:null;
?>
<div class="site-static-page">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    if(!empty($content)){
      echo $content;
    }
    else{
      include($pagePath);
    }
    ?>

</div>
