<?php

namespace frontend\controllers;

use Yii;
use common\models\User;
use common\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\ContentNegotiator;
use yii\web\Response;

class UserController extends \yii\web\Controller
{

  /**
   * @inheritdoc
   */
  public function behaviors()
  {
      return [
          'verbs' => [
              'class' => VerbFilter::className(),
              'actions' => [
                  'autocomplete' => ['GET'],
              ],
          ],
          'bootstrap' => [
            'class' => ContentNegotiator::className(),
              'only' => ['autocomplete'],
              'formats' => [ 'application/json' => Response::FORMAT_JSON ],
                'languages' => [
                  //'ru-RU',
                  //'en-US'
                  Yii::$app->language,
              ],
          ],
      ];
  }

  /**
   * Lists all User models.
   * @return mixed
   */
  public function actionIndex()
  {
      $searchModel = new UserSearch();
      $params = Yii::$app->request->queryParams;
      $dataProvider = $searchModel->search($params);

      return $this->render('index', [
          'searchModel' => $searchModel,
          'dataProvider' => $dataProvider,
          'params' => $params,
      ]);
  }

  /**
   * Displays a single User model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id)
  {
      return $this->render('view', [
          'model' => $this->findModel($id),
      ]);
  }

  /**
   * Displays search results
   * @param string $phrase
   * @param string | array $field
   * @return mixed
   */
  public function actionSearch($term=null,$field=null)
  {
      $searchParams = [];
      $userModel = new User();
      $searchModel = new UserSearch();
      if($field=$field?$field:(Yii::$app->request->post('field','username'
      ))){
        if($term=$term?$term:(Yii::$app->request->post('term'))){
          $searchParams['UserSearch'][$field] = $term;
        }
      }

      $dataProvider = $searchModel->search($searchParams);

      return $this->render('search', [
          'searchModel' => $searchModel,
          'dataProvider' => $dataProvider,
          'term' => $term,
          'url' => Yii::$app->request->url,
          'title' => Yii::t('user', 'Search'),
          'radioList' => ['id'=>'id','username'=>'username','email'=>'email','status'=>'status'],//$userModel::attributeLabels(),
          'field' => $field,
      ]);
  }

  /**
   * Displays ajax results for autocomplete purpose
   * @param string $term
   * @return mixed
   */
  public function actionAutocomplete($term)
  {
    $field=Yii::$app->request->get('field','title');
    return User::find()
      //->select(['id as value', 'title as label'])
      //->select(['title as value', 'title as label'])
      //->andFilterWhere(['like', 'title', Yii::$app->request->get('term')])
      ->select([$field.' as value', $field.' as label'])
      ->andFilterWhere(['like', $field, Yii::$app->request->get('term')])
      ->distinct()
      ->asArray()
      ->all();
  }

  /**
   * Finds the User model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return User the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id)
  {
      if (($model = User::find()->with('thumbnail')->andWhere(['id' => $id])->one()) !== null) {
          return $model;
      } else {
          throw new NotFoundHttpException('The requested page does not exist.');
      }
  }

}
